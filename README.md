# Publishing a Website on Bitbucket Cloud #

You can use Bitbucket to host a static website. A static website contains coded HTML pages with fixed content. Websites hosted in this way have the bitbucket.io domain in their URL, for example, https://tortoisehg.bitbucket.io.

Publishing a static website on Bitbucket Cloud requires you to combine your account's name with the bitbucket.io domain suffix. So, your account's name must be acceptable to your DNS service. Upper case characters and special characters are typically not acceptable. For example, if your account's name is happy_cat, you would need to create a new account with a username of happycat to use this feature because underscores are not allowed in DNS hostnames. Your repository name would be happycat.bitbucket.io and the published static website is reached by this URL: https://happycat.bitbucket.io

https://confluence.atlassian.com/bitbucket/publishing-a-website-on-bitbucket-cloud-221449776.html#PublishingaWebsiteonBitbucketCloud-ConfigureaWebsiteforHosting